--
---- a jointure relie 2 tables correspondance entre la clé primaire et la clé étrangere.
--

SELECT
    b.id,
    content,
    user_id,
    nickname
FROM
    blog b -- alias
INNER JOIN
    user u
ON
    user_id = u.id;