-- vaut commentaire

-- 
-- SELECT
--
-- * c'est un 'joker' qui va prendre toutes les colonnes

SELECT 
    *
FROM
    user;

-- preciser les colonnes

SELECT
    nickname,
    id
FROM 
    user;

-- Limit 

SELECT
    nickname,
    id
FROM 
    user
LIMIT 2;

-- nom de colonne

SELECT
    nickname as 'Pseudo',
    id as 'id de l utlisateur'
FROM 
    user
LIMIT 2;

-- ORDER BY
SELECT
    nickname as 'Pseudo',
    id as 'id de l utlisateur'
FROM 
    user
ORDER BY 
    id DESC
;

-- clause WHERE

SELECT
    nickname as 'Pseudo',
    id as 'id de l utlisateur'
FROM 
    user
WHERE 
    id = 2; -- retourne l'utilsateur avec l'id 2

SELECT
    nickname as 'Pseudo',
    id as 'id de l utlisateur'
FROM 
    user
WHERE 
    nickname  like '%_enobi%'; -- retourne les lignes qui contienne enobi
    -- le like recherche une valeur qui correspond au parametre fourni
    -- le _ remplace "une" lettre (utile pour les accents et les majuscules)
    -- le % permet d'indiquer ce qui y a avant ou après 

SELECT 
    *
FROM 
   user
WHERE
    id in (2,3); -- in va chercher dans l'ensemble des valeurs entre parentheses

-- IS NULL / IS NOT NULL

SELECT 
    *
FROM 
   user
WHERE
    profile_pic IS NULL; -- retourne les lignes où le champ profile_pic est null

-- condition
SELECT 
    *
FROM 
   blog
WHERE
     alikes >= 10; -- retourne les blogs qui ont 10 ou + like

SELECT 
    *
FROM 
   blog
WHERE
     alikes <> 10; -- différent de 10 like

-- Between

SELECT 
    *
FROM 
   blog
WHERE
     alikes 
BETWEEN 
    5 
AND 
    10;

--
-- CREATE -> INSERT
--

INSERT INTO 
    blog (content, user_id, alikes)
VALUES 
    ('Coucou ', 3, 5); 
    -- insert une nouvelle ligne, id est geré par MySQL; idem pour date_created
    -- /!\ L'ordre des champs est important

--
-- UPDATE
--
UPDATE
    `user`
SET
    nickname = 'Utilisateur3'
WHERE
    id = 3;

--
-- DELETE
-- 
DELETE FROM 
    user
WHERE 
    id = 4; -- supprime l'utilisateur avec l'id 4, Il n'y a pas d'avertissement à l'execution

-- Toutes ces requêtes sont ce que l'ont appel le CRUD (CREATE READ UPDATE DELETE)