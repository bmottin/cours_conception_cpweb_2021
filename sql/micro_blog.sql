-- phpMyAdmin SQL Dump
-- version 5.0.1
-- https://www.phpmyadmin.net/
--
-- Hôte : localhost
-- Généré le : jeu. 06 mai 2021 à 14:51
-- Version du serveur :  5.7.24
-- Version de PHP : 7.2.19

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données : `micro_blog`
--

-- --------------------------------------------------------

--
-- Structure de la table `blog`
--

CREATE TABLE `blog` (
  `id` int(11) NOT NULL,
  `content` varchar(128) NOT NULL,
  `user_id` int(11) NOT NULL,
  `alikes` int(11) NOT NULL,
  `date_created` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `blog`
--

INSERT INTO `blog` (`id`, `content`, `user_id`, `alikes`, `date_created`) VALUES
(1, 'Coucou, ça roule bien', 1, 10, '2020-09-18 09:30:02'),
(2, 'Aujourd\'hui, j\'ai mangé une noix de coco', 2, 1, '2020-09-18 09:40:02'),
(3, 'Yatta', 1, 5, '2020-09-18 10:20:44'),
(4, 'Allez hop on y va, en route pour l\'aventure !!!', 1, 8, '2020-09-18 10:21:07'),
(5, 'Je vais lui faire une offre qu\'il ne pourra pas refuser. ', 1, 6, '2020-09-18 12:23:00'),
(6, 'Yop', 1, 0, '2020-09-18 13:09:30'),
(7, 'Nous ne devons plus être au Kansas, qu\'en penses-tu Toto ?', 1, 20, '2020-09-18 13:09:39'),
(8, 'Que la Force soit avec toi.', 2, 4, '2020-09-18 13:10:21'),
(9, 'C\'est à moi que tu parles ? ', 2, 0, '2020-09-18 14:34:05'),
(10, 'j\'aime l\'odeur du napalm au petit matin. ', 2, 0, '2020-09-18 14:35:40'),
(11, 'E.T téléphone maison. ', 2, 2, '2020-09-18 14:38:28'),
(12, 'J\'ai dégusté son foie avec des fèves au beurre, et un excellent chianti', 2, 0, '2020-09-18 14:38:51'),
(13, 'Youpla', 2, 1, '2020-09-30 15:38:30'),
(14, 'héhé', 1, 0, '2021-03-29 12:58:39');

-- --------------------------------------------------------

--
-- Structure de la table `user`
--

CREATE TABLE `user` (
  `id` int(11) NOT NULL,
  `nickname` varchar(50) NOT NULL,
  `email` varchar(50) NOT NULL,
  `password` varchar(50) NOT NULL,
  `profile_pic` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `user`
--

INSERT INTO `user` (`id`, `nickname`, `email`, `password`, `profile_pic`) VALUES
(1, 'BenKenobi', 'ben@ben.com', 'admin123', NULL),
(2, 'Le chevalier Noir', 'eric.chapman@bbc.com', 'admin123', 'dark_night.jpg'),
(3, 'Test', 'test@test.com', 'admin123', NULL),
(4, 'Test', 'test@test.com', 'admin123', NULL);

--
-- Index pour les tables déchargées
--

--
-- Index pour la table `blog`
--
ALTER TABLE `blog`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT pour les tables déchargées
--

--
-- AUTO_INCREMENT pour la table `blog`
--
ALTER TABLE `blog`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;

--
-- AUTO_INCREMENT pour la table `user`
--
ALTER TABLE `user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
