--
-- FONCTION
--

-- SUM
SELECT 
    user_id,
    SUM(alikes)
FROM
    blog
WHERE
    user_id = 2; -- je retourne la somme des likes de l'utilisateur 2

-- GROUP BY / Aggregation
-- Afficher la quantité de poste par utilisateur
SELECT
    user_id,
    COUNT(user_id) -- compte le nombre de ligne
FROM
    blog -- depuis la table article
GROUP BY -- regroupe par groupe logique ici par utilisateur
    user_id;
-- 

-- Average / AVG
SELECT
    user_id, 
    COUNT(id), -- compte le nombre de post
    SUM(alikes), -- fait la somme de like
    AVG(alikes) -- la moyenne des likes
FROM
    blog
WHERE
    user_id in (2,3)
GROUP BY
    user_id;

